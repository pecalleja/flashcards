from collections import OrderedDict, defaultdict
from dataclasses import dataclass
import random
import os
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--import_from")
parser.add_argument("--export_to")


@dataclass()
class Card:
    term: str
    definition: str

    def __str__(self):
        return f"{self.term}:{self.definition}"

    def __repr__(self):
        return f"{self.term}:{self.definition}"


class CardList:
    def __init__(self):
        self.cards = OrderedDict()
        self.inverse_cards = OrderedDict()
        self.error_cards = defaultdict(int)

    def append(self, other: Card):
        self.cards[other.term] = other.definition
        self.inverse_cards[other.definition] = other.term

    def remove(self, term: str):
        existing = self.cards.pop(term, None)
        if existing:
            self.inverse_cards.pop(existing, None)

    def fail(self, term):
        self.error_cards[term] += 1

    def reset_errors(self):
        self.error_cards = defaultdict(int)

    def get_most_fail(self):
        if not self.error_cards:
            return [], 0
        max_value = max(self.error_cards.values())
        cards = []
        for term, errors in self.error_cards.items():
            if errors == max_value:
                cards.append(term)
        return cards, max_value

    def keys(self):
        return self.cards.keys()

    def values(self):
        return self.cards.values()

    def items(self):
        yield from [Card(term=key, definition=value) for key, value in self.cards.items()]

    def get_answer(self, text, side: str):
        if side == "term":
            return self.cards.get(text)
        else:
            return self.inverse_cards.get(text)

    def existing(self, side: str):
        if side == "term":
            return self.keys()
        else:
            return self.values()


class CardsGame:
    cards: CardList

    def __init__(self, import_from, export_to):
        self.cards = CardList()
        self.log_buffer = []
        self.import_from = import_from
        self.export_to = export_to

    def print(self, string):
        self.log_buffer.append(string + "\n")
        print(string)

    def input(self):
        text = input()
        self.log_buffer.append(f"> {text}\n")
        return text

    def log_command(self):
        file_name = self.user_input_file()
        with open(file_name, "w", encoding="utf-8") as file_input:
            file_input.writelines(self.log_buffer)
        self.print("The log has been saved.")

    def user_input_file(self):
        self.print("File name:")
        file_name = self.input()
        return file_name

    def add_command(self):
        self.print("The card:")
        term = self.input_card_side("term")
        self.print("The definition of the card:")
        definition = self.input_card_side("definition")
        card = Card(term=term, definition=definition)
        self.cards.append(card)
        self.print(f'The pair ("{term}":"{definition}") has been added')
        return card

    def import_command(self):
        if self.import_from:
            file_name = self.import_from
        else:
            file_name = self.user_input_file()
        if os.path.exists(file_name):
            with open(file_name, "r", encoding="utf-8") as file_input:
                all_lines = file_input.readlines()
                for card in all_lines:
                    term, definition = card.split(":")
                    if term in self.cards.keys():
                        self.cards.remove(term)
                    self.cards.append(Card(
                        term, definition[:-1]
                    ))
                self.print(f"{len(all_lines)} cards have been loaded.")
        else:
            self.print("File not found.")

    def export_command(self):
        if self.export_to:
            file_name = self.export_to
        else:
            file_name = self.user_input_file()
        all_cards = [str(card)+"\n" for card in self.cards.items()]
        with open(file_name, "w", encoding="utf-8") as file_input:
            file_input.writelines(all_cards)
        self.print(f"{len(all_cards)} cards have been saved.")

    def remove_command(self):
        self.print("Which card?")
        term = self.input()
        if term not in self.cards.existing("term"):
            self.print(f'Can\'t remove "{term}": there is no such card.')
        else:
            self.cards.remove(term)
            self.print("The card has been removed.")

    def exit_command(self):
        self.print("Bye bye!")
        if self.export_to:
            self.export_command()
        exit()

    def hardest_command(self):
        error_cards, errors = self.cards.get_most_fail()
        if errors == 0:
            self.print("There are no cards with errors.")
        elif len(error_cards) == 1:
            self.print(f'The hardest card is "{error_cards[0]}". You have {errors} errors answering it')
        elif len(error_cards) > 1:
            text_cards = '", "'.join(error_cards)
            self.print(f'The hardest cards are "{text_cards}". You have {errors} errors answering them.')

    def reset_command(self):
        self.cards.reset_errors()
        self.print("Card statistics have been reset.")

    def start(self):
        if self.import_from:
            self.import_command()
        while True:
            self.print("Input the action (add, remove, import, export, ask, exit, log, hardest card, reset stats):")
            command = self.input().split()[0]
            execute = getattr(self, f"{command}_command")
            if execute:
                execute()
            else:
                self.print("Invalid command")
            self.print("")

    def ask_command(self):
        self.print("How many times to ask?")
        amount = int(self.input())
        all_cards = list(self.cards.keys())
        if amount <= len(all_cards):
            chosen = random.sample(all_cards, amount)
        else:
            chosen = random.choices(all_cards, k=amount)
        for term in chosen:
            self.print(f'Print the definition of "{term}":')
            user_answer = self.input()
            right_answer = self.cards.get_answer(term, "term")
            if right_answer == user_answer:
                self.print("Correct!")
            else:
                self.cards.fail(term)
                if user_answer in self.cards.values():
                    answer_term = self.cards.get_answer(user_answer, "definition")
                    self.print(f'Wrong. The right answer is "{right_answer}", but your definition is correct for "{answer_term}"')
                else:
                    self.print(f'Wrong. The right answer is "{right_answer}".')

    def input_card_side(self, side: str):
        while True:
            text = self.input()
            if text not in self.cards.existing(side):
                break
            else:
                self.print(f'The {side} "{text}" already exists. Try again:')
        return text


args = parser.parse_args()
CardsGame(
    import_from=args.import_from,
    export_to=args.export_to
).start()
